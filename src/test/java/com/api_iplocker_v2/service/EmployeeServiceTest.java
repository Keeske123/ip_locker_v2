package com.api_iplocker_v2.service;

import com.api_iplocker_v2.interfaces.IEmployeeRepository;
import com.api_iplocker_v2.repositories.EmployeeRepository;
import com.api_iplocker_v2.models.Employee_DTO;
import com.api_iplocker_v2.serviceInterface.IEmployeeService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class EmployeeServiceTest {



    @Test
    void getConnections() {

        EmployeeRepository ier = new EmployeeRepository();
        List<Employee_DTO> testList = ier.GetConnections();

        assertFalse(testList.isEmpty(), "List Has Items");
    }

    @Test
    void addConnection() {
        EmployeeRepository ier = new EmployeeRepository();

        boolean testValue = ier.AddConnection(0);
        assertTrue(testValue, "Added Connection test was Succesful");
    }
}