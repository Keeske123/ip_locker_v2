package com.api_iplocker_v2;
import com.api_iplocker_v2.interfaces.IEmployeeRepository;
import com.api_iplocker_v2.mock_repositories.EmployeeRepository;
import com.api_iplocker_v2.models.Employee_DTO;
import com.api_iplocker_v2.serviceInterface.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;

import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ApiIpLockerV2ApplicationTests {

    @Test
    void getConnections() {

        EmployeeRepository ier = new EmployeeRepository();
        List<Employee_DTO> testList = ier.GetConnections();

        assertFalse(testList.isEmpty(), "List Has Items");
    }

    @Test
    void addConnection() {
        EmployeeRepository ier = new EmployeeRepository();

        boolean testValue = ier.AddConnection(0);
        assertTrue(testValue, "Added Connection test was Succesful");
    }

}
