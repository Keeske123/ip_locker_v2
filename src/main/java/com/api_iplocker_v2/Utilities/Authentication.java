package com.api_iplocker_v2.Utilities;

import org.apache.tomcat.util.codec.binary.Base64;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Authentication {
    private String Token;

    public static String GenerateToken(){
        Random random = ThreadLocalRandom.current();
        byte[] r = new byte[32]; //Means 2048 bit
        random.nextBytes(r);
        String token = Base64.encodeBase64String(r);
        token.replace('=','a');
        token.replace('/','B');
        return token;
    }
}
