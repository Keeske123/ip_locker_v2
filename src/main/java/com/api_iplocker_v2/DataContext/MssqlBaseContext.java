package com.api_iplocker_v2.DataContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MssqlBaseContext {

    @Autowired
    private Environment env;
    protected Connection getDatabaseConnection() {

        //String url = env.getProperty("spring.datasource.url");
        //String username = env.getProperty("spring.datasource.username");
        //String password = env.getProperty("spring.datasource.password");

        String url = "jdbc:sqlserver://mssql.fhict.local;database=dbi410945_dbimfa";
        String username = "dbi410945_dbimfa";
        String password = "Hr89qLZ7";
        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
            return connection;
        }
        catch (SQLException e) {
            throw new IllegalStateException("JDBC driver failed to connect to the database " + url + ".", e);
        }
    }
}
