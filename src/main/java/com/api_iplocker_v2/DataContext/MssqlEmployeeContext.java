package com.api_iplocker_v2.DataContext;

import com.api_iplocker_v2.models.Employee_DTO;
import com.api_iplocker_v2.models.User_DTO;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MssqlEmployeeContext extends MssqlBaseContext {

    List<Employee_DTO> employeeDtoList;

    List<Employee_DTO> _tempEmployeeList;

    int _tempIpID = -1, _tempEmployeeID = -1;


    public void DeleteConnection(int connectionID){
        String query = "DELETE FROM connections WHERE connection_id = " + connectionID;
        Connection con = getDatabaseConnection();
        try(Statement stmt = con.createStatement()){
            stmt.executeUpdate(query);
            ResultSet rs = stmt.executeQuery(query);
            rs.close();
        } catch (SQLException e) {
            System.out.print(e.toString());
        }

    }

    public List<Employee_DTO> GetConnections() {
        String query = "SELECT * FROM currentConnections";
        Connection con = getDatabaseConnection();
        employeeDtoList = new ArrayList<Employee_DTO>();
        try (Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                employeeDtoList.add(new Employee_DTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),rs.getString(5)));
            }

            con.commit();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            employeeDtoList.add(new Employee_DTO(0, e.toString(), e.getSQLState()));
        } return employeeDtoList;

    }

    public Boolean AddConnection(int connectionId) {
        Employee_DTO insertObject = GetConnectionByID(connectionId);
        boolean flag = false;

        //if not known, insert immediatly, return true
        if (KnownNetwork(insertObject.currentIp)) {

            //if not known, insert immediatly, return true
            if (EmployeeExist(insertObject.employeeName)) {

                flag = CreateConnection();


            }
        }
        return flag;

    }

    private boolean CreateConnection() {
        boolean flag = false;
        String query = "INSERT INTO connections (emp_id, kn_id, connected) VALEUES (" + _tempEmployeeID + ", " + _tempIpID + ", " + true + ")";
        Connection con = getDatabaseConnection();

        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate(query);

            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            employeeDtoList.add(new Employee_DTO(0, e.toString(), e.getSQLState()));
        } return flag;
    }

    private boolean EmployeeExist(String employeeName) {
        String query = "SELECT emp_id FROM employees WHERE emp_name = " + employeeName;
        Connection con = getDatabaseConnection();

        try (Statement stmt = con.createStatement()) {
            ResultSet rs = stmt.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    _tempEmployeeID = rs.getInt(1);
                }
            } else {
                InsertNewEmployee(employeeName);
            }

            con.commit();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            _tempEmployeeID = -1;
        } if (_tempEmployeeID >= 0)
            return true;
        else
            return false;
    }

    private void InsertNewEmployee(String employeeName) {
        boolean flag = false;
        String query = "INSERT INTO employees (emp_name) VALEUES (" + employeeName + ")";
        Connection con = getDatabaseConnection();

        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate(query);

            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            // employeeDtoList.add(new Employee_DTO(0, e.toString(), e.getSQLState()));
        }
        EmployeeExist(employeeName);
    }

    private boolean KnownNetwork(String currentIp) {
        String query = "SELECT kn_id FROM kn WHERE kn_name = ?";
        Connection con = getDatabaseConnection();

        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1,currentIp);
            ResultSet rs = stmt.executeQuery();

            if (rs != null) {
                boolean subflag = false;
                while (rs.next()) {
                    _tempIpID = rs.getInt(1);
                    subflag = true;
                }
                if (!subflag){
                    InsertNewNetwork(currentIp);
                }
            } else {
                InsertNewNetwork(currentIp);
            }

            con.commit();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            _tempIpID = -1;
        }  if (_tempIpID >= 0)
            return true;
        else
            return false;
    }

    private void InsertNewNetwork(String currentIp) {
        //boolean flag = false;
        String query = "INSERT INTO kn (kn_name) VALUES(?)";
        Connection con = getDatabaseConnection();

        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1,currentIp);
            stmt.executeUpdate();

            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            // employeeDtoList.add(new Employee_DTO(0, e.toString(), e.getSQLState()));
        }
        KnownNetwork(currentIp);
    }

    public Employee_DTO GetConnectionByID(int connectionId) {
        String query = "SELECT * FROM currentConnections WHERE current_id = " + connectionId;
        Connection con = getDatabaseConnection();
        Employee_DTO employee = null;
        _tempEmployeeList = new ArrayList<>();
        try (Statement stmt = con.createStatement()) {
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                employee = new Employee_DTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            }

            con.commit();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.toString());
            employee =  new Employee_DTO(0, e.toString(), e.getSQLState(), "", "");
        }
        finally {
            return employee;
        }
    }


}
