package com.api_iplocker_v2.DataContext;

import com.api_iplocker_v2.Utilities.Authentication;
import com.api_iplocker_v2.interfaces.IUserRepository;
import com.api_iplocker_v2.models.User_DTO;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Repository
public class MssqlUserContext extends MssqlBaseContext {

    User_DTO _user;

    public MssqlUserContext() {

    }


    public User_DTO LoginUser(String username) throws SQLException {

        String query = "SELECT * FROM users WHERE username = @username";
        Connection con = getDatabaseConnection();


        try (Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                _user = new User_DTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(3));
                System.out.println(username);
            }

            con.commit();
        } catch (SQLException e) {
            System.out.println(e.toString());
            _user = null;
        } finally {

            con.close();
//SetUserToken(_user);
            GetUserToken(_user);
            return _user;
        }

    }

    private void GetUserToken(User_DTO user) throws SQLException {
        user.setToken(Authentication.GenerateToken());
        SetUserToken(user);
    }

    private void SetUserToken(User_DTO user) throws SQLException {
        //int tokenID = 0;
        int tokenID = GetUserTokenID(user.getUsername());
        String query = "UPDATE tokens SET token = ? WHERE id = ?";
        //String query = "UPDATE tokens SET token = ? WHERE id = 0";
        Connection con = getDatabaseConnection();
        try {
            PreparedStatement statement = con.prepareStatement(query);
            statement.setString(1, user.getToken());
            statement.setInt(2, tokenID);
            statement.executeUpdate();
            con.commit();
        } catch (SQLException e) {
            System.out.println(e.toString());
        } finally {
            con.close();
        }
    }

    private int GetUserTokenID(String username) throws SQLException {
        int tokenid = -1;
        String query = "SELECT id FROM tokens WHERE id = (SELECT tokenID FROM users WHERE username = ?)";
        Connection con = getDatabaseConnection();

        try {
            PreparedStatement statement = con.prepareStatement(query);
            statement.setString(1, username);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tokenid = rs.getInt(1);
                System.out.println(tokenid);
            }
            con.commit();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            con.close();
            return tokenid;
        }
    }


    public User_DTO LoginUser(String username, String password) throws SQLException {
        String query = "SELECT * FROM users WHERE username = ? AND password = ?";
        Connection con = getDatabaseConnection();


        try {
            PreparedStatement statement = con.prepareStatement(query);
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                _user = new User_DTO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getBoolean(4));
                System.out.println(username);
            }

            con.commit();
        } catch (SQLException e) {
            System.out.println(e.toString());
            _user = null;
        } finally {
            con.close();
            GetUserToken(_user);
            return _user;
        }
    }

    public User_DTO SetUser(String username, String password, boolean isSuperAdmin) {
        return null;
    }
}
