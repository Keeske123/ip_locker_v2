package com.api_iplocker_v2.interfaces;

import com.api_iplocker_v2.models.Employee_DTO;

import java.util.List;

public interface IEmployeeRepository {
    public List<Employee_DTO> GetConnections();
    public boolean AddConnection(int id);

   public Employee_DTO GetConnection(int connectionID);
}
