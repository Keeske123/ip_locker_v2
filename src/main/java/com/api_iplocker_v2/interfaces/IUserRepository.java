package com.api_iplocker_v2.interfaces;

import com.api_iplocker_v2.models.User_DTO;

import java.sql.SQLException;

public interface IUserRepository {
    public User_DTO LoginUser(String username);
    public User_DTO LoginUser(String username, String password) throws SQLException;
    public User_DTO SetUser(String username, String password, boolean isSuperAdmin);
}
