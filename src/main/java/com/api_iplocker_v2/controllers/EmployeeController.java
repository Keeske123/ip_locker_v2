package com.api_iplocker_v2.controllers;

import com.api_iplocker_v2.models.Employee_DTO;
import com.api_iplocker_v2.models.Employee_IP_DTO;
import com.api_iplocker_v2.serviceInterface.IEmployeeService;
import com.api_iplocker_v2.serviceInterface.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin("http://localhost:3000")
@RequestMapping("/Employees")
@RestController
public class EmployeeController {

    //Dependency Injection!
    @Autowired
    IEmployeeService ies;

   // @Autowired
   // IUserService ius;


    @GetMapping("/GetConnections")
    public List<Employee_DTO> GetConnections() {
        return ies.GetConnections();
    }

    @PostMapping("/AddConnection")
    public void AddConnection(@RequestParam int id) {
//HAS INFINITE LOOP

       ies.AddConnection(id);
    }

    @PostMapping("/CreateRandomConnection")
    public void CreateRandomConnection(){

    }

//@PostMapping
//public boolean RemoveConnection(String employeeName, String IP) {
//
//   return true;
//}

  // @PostMapping
  // public void UpdateUser(@RequestParam int id){

  // }

    @GetMapping("/GetConnectionInfo")
    public List<Employee_IP_DTO> GetConnectionInfo(@RequestParam String connectionID){

        List<Employee_IP_DTO> returnList = new ArrayList<>();
        returnList.add(ies.GetConnecionInfo(Integer.parseInt(connectionID)));
        return returnList;
    }

    public List<Employee_DTO> SearchFilterConnections() {
        return new ArrayList<Employee_DTO>();
    }
}
