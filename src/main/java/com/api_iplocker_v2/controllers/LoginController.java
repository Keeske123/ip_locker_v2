package com.api_iplocker_v2.controllers;

import com.api_iplocker_v2.models.User_DTO;
import com.api_iplocker_v2.serviceInterface.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@CrossOrigin("http://localhost:3000/")
@RequestMapping("/Login")
public class LoginController {

    @Autowired
    private IUserService service;

    //IUserContext uc = new UserRepository();

    //@PostMapping("/")
    //public void PostUser(@RequestBody User_DTO user) {
    //    service.LoginUser(user.getUsername(),user.getPassword());
//
    //    //return uc.GetUser(user.getUsername(), user.getPassword());
    //}

    @GetMapping("")
    public User_DTO PostUser(@RequestParam String un, @RequestParam String pw) throws SQLException {
       return service.LoginUser(un, pw);


    }

    public boolean Register() {
        return true;
    }
}
