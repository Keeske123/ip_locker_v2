package com.api_iplocker_v2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiIpLockerV2Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiIpLockerV2Application.class, args);
	}

}
