package com.api_iplocker_v2.repositories;

import com.api_iplocker_v2.DataContext.MssqlUserContext;
import com.api_iplocker_v2.interfaces.IUserRepository;
import com.api_iplocker_v2.models.User_DTO;

//import com.api_iplocker_v2.Entities.User;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

@Repository
public class UserRepository implements IUserRepository {

    MssqlUserContext muc = new MssqlUserContext();

    @Override
    public User_DTO LoginUser(String username) {
        return null;
    }

    @Override
    public User_DTO LoginUser(String username, String password) throws SQLException {
        return muc.LoginUser(username, password);

    }

    @Override
    public User_DTO SetUser(String username, String password, boolean isSuperAdmin) {
        return null;
    }
}
