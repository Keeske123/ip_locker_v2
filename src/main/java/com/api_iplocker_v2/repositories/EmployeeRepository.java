package com.api_iplocker_v2.repositories;

import com.api_iplocker_v2.DataContext.MssqlEmployeeContext;
import com.api_iplocker_v2.interfaces.IEmployeeRepository;
import com.api_iplocker_v2.models.Employee_DTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeRepository implements IEmployeeRepository {

   MssqlEmployeeContext mec = new MssqlEmployeeContext();


    @Override
    public List<Employee_DTO> GetConnections() {
        return mec.GetConnections();
    }

    @Override
    public boolean AddConnection(int id) {
        return mec.AddConnection(id);
    }

    @Override
    public Employee_DTO GetConnection(int connectionID) {
        return mec.GetConnectionByID(connectionID);

    }
}
