package com.api_iplocker_v2.service;

import com.api_iplocker_v2.interfaces.IEmployeeRepository;
import com.api_iplocker_v2.models.Employee_DTO;
import com.api_iplocker_v2.models.Employee_IP_DTO;
import com.api_iplocker_v2.models.TrustedIP;
import com.api_iplocker_v2.serviceInterface.IEmployeeService;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    IEmployeeRepository iec;

    @Override
    public List<Employee_DTO> GetConnections() {
        return iec.GetConnections();
    }

    @Override
    public boolean AddConnection(int id) {
        return iec.AddConnection(id);
    }

    @Override
    public Employee_IP_DTO GetConnecionInfo(int connectionID) {
        Employee_IP_DTO employee_ip_dto = new Employee_IP_DTO(iec.GetConnection(connectionID));
       // employee_ip_dto.employee = iec.GetConnection(connectionID);
        //employee_ip_dto = new TrustedIP(employee_ip_dto.employee.connectionInfo);
        System.out.println(employee_ip_dto.employeeName);
        return  employee_ip_dto;
    }

    private TrustedIP GetIpInfo(String currentIp) {


        return null;
    }
}
