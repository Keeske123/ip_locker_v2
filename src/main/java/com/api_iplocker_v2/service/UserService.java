package com.api_iplocker_v2.service;

import com.api_iplocker_v2.interfaces.IUserRepository;
import com.api_iplocker_v2.models.User_DTO;
import com.api_iplocker_v2.serviceInterface.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class UserService implements IUserService {

    @Autowired
    IUserRepository iUserRepository;


    @Override
    public User_DTO LoginUser(String username) {
        return iUserRepository.LoginUser(username);
    }

    @Override
    public User_DTO LoginUser(String username, String password) throws SQLException {
        return iUserRepository.LoginUser(username,password);
    }

    @Override
    public User_DTO SetUser(String username, String password, boolean isSuperAdmin) {
        return iUserRepository.SetUser(username,password,isSuperAdmin);
    }
}
