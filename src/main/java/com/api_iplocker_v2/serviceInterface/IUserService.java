package com.api_iplocker_v2.serviceInterface;

import com.api_iplocker_v2.models.User_DTO;

import java.sql.SQLException;

public interface IUserService {
    User_DTO LoginUser(String username);
    User_DTO LoginUser(String username, String password) throws SQLException;
    User_DTO SetUser(String username, String password, boolean isSuperAdmin);

}
