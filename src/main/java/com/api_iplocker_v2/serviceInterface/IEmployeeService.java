package com.api_iplocker_v2.serviceInterface;

import com.api_iplocker_v2.models.Employee_DTO;
import com.api_iplocker_v2.models.Employee_IP_DTO;

import java.util.List;

public interface IEmployeeService {
    public List<Employee_DTO> GetConnections();
    public boolean AddConnection(int id);
    public Employee_IP_DTO GetConnecionInfo(int connectionID);
}
