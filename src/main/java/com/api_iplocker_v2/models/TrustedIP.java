package com.api_iplocker_v2.models;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonObject;
import org.apache.tomcat.util.json.JSONParser;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class TrustedIP {
        public String ip;
        public boolean is_eu;
        public String city;
        public String region;
        public String region_code;
        public String country_name;
        public String country_code;
        public String continent_name;
        public String continent_code;
        public double latitude;
        public double longitude;
        public String postal;
        public String calling_code;




        public TrustedIP (String connectionInfo) {

                JsonArray test = new JsonArray();
                test.add(connectionInfo);
                List<String> testList = new ArrayList<String>();

                for (int i = 0; i < test.stream().count(); i++) {
                        testList.add(test.getString(i));
                }
                // Creating String array as our
                // final required output
                int size = testList.size();
                String[] stringArray
                        = testList.toArray(new String[size]);

                // Printing the contents of String array
                System.out.print("Output String array will be : ");
                for (String s : stringArray) {
                        String temp = s.split(",")[0].split(":")[1];
                        ip = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[1].split(":")[1];
                        is_eu = (temp.substring(1, temp.length()).equals("true"));

                        temp =  s.split(",")[2].split(":")[1];
                        city = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[3].split(":")[1];
                        region = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[4].split(":")[1];
                        region_code = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[5].split(":")[1];
                        country_name = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[6].split(":")[1];
                        country_code = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[7].split(":")[1];
                        continent_name = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[8].split(":")[1];
                        continent_code = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[9].split(":")[1];
                        latitude = Double.parseDouble(temp.substring(1, temp.length()));

                        temp =  s.split(",")[10].split(":")[1];
                        longitude = Double.parseDouble(temp.substring(1, temp.length()));

                        temp =  s.split(",")[11].split(":")[1];
                        postal = temp.substring(2,temp.length()-1);

                        temp =  s.split(",")[12].split(":")[1];
                        calling_code = temp.substring(2,temp.length()-1);
                     break;
                }
        }
}
