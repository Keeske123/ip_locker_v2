package com.api_iplocker_v2.models;

import java.util.List;

public class IP_EmployeeDTO {
    public TrustedIP IP;
    public List<Employee_DTO> employeeDTOS;
}
