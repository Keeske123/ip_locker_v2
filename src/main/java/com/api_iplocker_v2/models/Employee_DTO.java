package com.api_iplocker_v2.models;

public class Employee_DTO {
    public int rowID;
    public String employeeName;
    public String currentIp;
    public String connectedSince;
    public String connectionInfo;

    public Employee_DTO(int rowID, String employeeName, String currentIp) {
        this.rowID = rowID;
        this.employeeName = employeeName;
        this.currentIp = currentIp;
    }

    public Employee_DTO(int rowID, String employeeName, String currentIp, String connectedSince, String connectionInfo) {
        this.rowID = rowID;
        this.employeeName = employeeName;
        this.currentIp = currentIp;
        this.connectedSince = connectedSince;
        this.connectionInfo = connectionInfo;
    }

}