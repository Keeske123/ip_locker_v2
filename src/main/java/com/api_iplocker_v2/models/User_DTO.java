package com.api_iplocker_v2.models;

public class User_DTO {
    private int userID;
    private String username;
    private String password;
    private boolean isSuperAdmin;
    private String token;

    public User_DTO(int userID, String username, String password, boolean isSuperAdmin){
        this.userID = userID;
        this.username = username;
        this.password = password;
        this.isSuperAdmin = isSuperAdmin;
    }


    public int getUserID(){ return userID;}

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isSuperAdmin() {
        return isSuperAdmin;
    }

    public String getToken(){return token;}
public void setToken(String value){
        this.token = value;
}

}
